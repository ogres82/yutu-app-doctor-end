import request from '@/utils/request'

// 查询购物车列表
export function listOmsCartItem(query) {
	return request({
		url: '/biz/OmsCartItem/list',
		method: 'get',
		params: query
	})
}

// 查询购物车详细
export function getOmsCartItem(omsCartItemId) {
	return request({
		url: '/biz/OmsCartItem/' + omsCartItemId,
		method: 'get'
	})
}

// 新增购物车
export function addOmsCartItem(data) {
	return request({
		url: '/biz/OmsCartItem',
		method: 'post',
		data: data
	})
}

// 修改购物车
export function updateOmsCartItem(data) {
	return request({
		url: '/biz/OmsCartItem',
		method: 'put',
		data: data
	})
}

// 删除购物车
export function delOmsCartItem(omsCartItemId) {
	return request({
		url: '/biz/OmsCartItem/' + omsCartItemId,
		method: 'delete'
	})
}
// 修改购物车
export function addOrUpdateOmsCartItem(data) {
	return request({
		url: '/biz/OmsCartItem/addOrUpdateOmsCartItem',
		method: 'put',
		data: data
	})
}