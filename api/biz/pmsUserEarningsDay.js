import request from '@/utils/request'

// 查询用户日收益列表
export function listPmsUserEarningsDay(query) {
	return request({
		url: '/biz/pmsUserEarningsDay/list',
		method: 'get',
		params: query
	})
}

// 查询用户日收益详细
export function getPmsUserEarningsDay(id) {
	return request({
		url: '/biz/pmsUserEarningsDay/' + id,
		method: 'get'
	})
}

// 新增用户日收益
export function addPmsUserEarningsDay(data) {
	return request({
		url: '/biz/pmsUserEarningsDay',
		method: 'post',
		data: data
	})
}

// 修改用户日收益
export function updatePmsUserEarningsDay(data) {
	return request({
		url: '/biz/pmsUserEarningsDay',
		method: 'put',
		data: data
	})
}

// 删除用户日收益
export function delPmsUserEarningsDay(id) {
	return request({
		url: '/biz/pmsUserEarningsDay/' + id,
		method: 'delete'
	})
}

// 查询最近一周或月的收益
export function weekEarnings(userId) {
	return request({
		url: '/biz/pmsUserEarningsDay/weekEarnings/' + userId,
		method: 'get',
	})
}