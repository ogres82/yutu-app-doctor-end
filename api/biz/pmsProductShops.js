// import request from '@/utils/request'

// // 查询商铺信息列表
// export function listPmsProductShops(query) {
//   return request({
//     url: '/biz/pmsProductShops/list',
//     method: 'get',
//     params: query
//   })
// }

// // 查询商铺信息详细
// export function getPmsProductShops(shopId) {
//   return request({
//     url: '/biz/pmsProductShops/' + shopId,
//     method: 'get'
//   })
// }

// // 新增商铺信息
// export function addPmsProductShops(data) {
//   return request({
//     url: '/biz/pmsProductShops',
//     method: 'post',
//     data: data
//   })
// }

// // 修改商铺信息
// export function updatePmsProductShops(data) {
//   return request({
//     url: '/biz/pmsProductShops',
//     method: 'put',
//     data: data
//   })
// }

// // 删除商铺信息
// export function delPmsProductShops(shopId) {
//   return request({
//     url: '/biz/pmsProductShops/' + shopId,
//     method: 'delete'
//   })
// }

// // 查询商铺信息列表
// export function selectShopList(query) {
//   return request({
//     url: '/biz/pmsProductShops/selectShopList',
//     method: 'get',
//     params: query
//   })
// }