import request from '@/utils/request'

// 查询系统医院-科室列表
export function listHospitalOrganization(query) {
	return request({
		url: '/biz/sysHospitalOrganization/list',
		method: 'get',
		params: query
	})
}

// 查询系统医院-科室详细
export function getHospitalOrganization(orgId) {
	return request({
		url: '/biz/sysHospitalOrganization/' + orgId,
		method: 'get'
	})
}

// 新增系统医院-科室
export function addHospitalOrganization(data) {
	return request({
		url: '/biz/sysHospitalOrganization',
		method: 'post',
		data: data
	})
}

// 修改系统医院-科室
export function updateHospitalOrganization(data) {
	return request({
		url: '/biz/sysHospitalOrganization',
		method: 'put',
		data: data
	})
}

// 删除系统医院-科室
export function delHospitalOrganization(orgId) {
	return request({
		url: '/biz/sysHospitalOrganization/' + orgId,
		method: 'delete'
	})
}
// 查询系统医院-获取医院信息
export function selectHospitalByAreaId(countyId) {
	return request({
		url: '/biz/sysHospitalOrganization/selectHospitalByAreaId/' + countyId,
		method: 'get',
	})
} // 查询系统医院-查询科室
export function selectDepartLists(query) {
	return request({
		url: '/biz/sysHospitalOrganization/selectDepartLists',
		method: 'get',
		params: query

	})
}