import request from '@/utils/request'

// 获取配工单列表
export function getListsByUserId(userId) {
	return request({
		url: '/biz/workOrder/getListsByUserId/' + userId,
		method: 'get',
	})
}
// 接收派工单
export function takeOrders(workerId) {
	return request({
		url: '/biz/workOrder/takeOrders/' + workerId,
		method: 'put',
	})
}
// 举手派工单
export function refuseOrders(workerId) {
	return request({
		url: '/biz/workOrder/refuseOrders/' + workerId,
		method: 'put',
	})
}
// 完成派工单
export function finishOrders(workerId) {
	return request({
		url: '/biz/workOrder/finishOrders/' + workerId,
		method: 'put',
	})
}