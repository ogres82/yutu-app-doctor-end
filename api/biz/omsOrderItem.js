import request from '@/utils/request'

// 查询订单和商品关系列表
export function listOmsOrderItem(query) {
	return request({
		url: '/biz/omsOrderItem/list',
		method: 'get',
		params: query
	})
}

// 查询订单和商品关系详细
export function getOmsOrderItem(id) {
	return request({
		url: '/biz/omsOrderItem/' + id,
		method: 'get'
	})
}

// 新增订单和商品关系
export function addOmsOrderItem(data) {
	return request({
		url: '/biz/omsOrderItem',
		method: 'post',
		data: data
	})
}

// 修改订单和商品关系
export function updateOmsOrderItem(data) {
	return request({
		url: '/biz/omsOrderItem',
		method: 'put',
		data: data
	})
}

// 删除订单和商品关系
export function delOmsOrderItem(id) {
	return request({
		url: '/biz/omsOrderItem/' + id,
		method: 'delete'
	})
}

// 查询我的未读报告count数
export function selectUnReadUserReports(query) {
	return request({
		url: '/biz/omsOrderItem/selectUnReadUserReports',
		method: 'get',
		params: query
	})
}


// 新增订单和商品关系
export function appAddOmsOrderItem(data) {
	return request({
		url: '/biz/omsOrderItem/addOmsOrderItem',
		method: 'post',
		data: data
	})
}
// 找一个一级或者二级推广员获取收益的订单信息
export function selectOmsOrderItemByCode(code) {
	return request({
		url: '/biz/omsOrderItem/orderItem/code/' + code,
		method: 'get',
	})
}