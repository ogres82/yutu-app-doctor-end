import request from '@/utils/request'

// 绑定一个条码
export function pmsOrderCodeBind(data) {
	return request({
		url: '/biz/pmsOrderCode/bind',
		method: 'post',
		data: data
	})
}