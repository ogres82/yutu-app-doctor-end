import request from '@/utils/request'

// 查询护士信息列表
export function listNurseInfo(query) {
  return request({
    url: '/biz/NurseInfo/list',
    method: 'get',
    params: query
  })
}

// 查询护士信息详细
export function getNurseInfo(nurseId) {
  return request({
    url: '/biz/NurseInfo/' + nurseId,
    method: 'get'
  })
}

// 新增护士信息
export function addNurseInfo(data) {
  return request({
    url: '/biz/NurseInfo',
    method: 'post',
    data: data
  })
}

// 修改护士信息
export function updateNurseInfo(data) {
  return request({
    url: '/biz/NurseInfo',
    method: 'put',
    data: data
  })
}

// 删除护士信息
export function delNurseInfo(nurseId) {
  return request({
    url: '/biz/NurseInfo/' + nurseId,
    method: 'delete'
  })
}
