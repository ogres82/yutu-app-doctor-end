import request from '@/utils/request'
 
 export function selectSmsSearchLogByColumn(query) {
   return request({
    url: '/biz/smslog/selectSmsSearchLogByColumn',
     method: 'get',
     params: query
   })
 }
 