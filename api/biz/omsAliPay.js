import request from '@/utils/request'

// 调用支付宝支付
export function aliPayApp(query) {
	return request({
		url: '/biz/OmsAliPay/aliPayApp',
		method: 'get',
		params: query
	})
}

// 调用支付宝支付
export function aliPayRefund(query) {
	return request({
		url: '/biz/OmsAliPay/aliPayRefund',
		method: 'get',
		params: query
	})
}