import request from '@/utils/request'

// 查询快递订单信息列表
export function listKdOrderInfo(query) {
	return request({
		url: '/biz/kdOrderInfo/list',
		method: 'get',
		params: query
	})
}

// 查询快递订单信息详细
export function getKdOrderInfo(id) {
	return request({
		url: '/biz/kdOrderInfo/' + id,
		method: 'get'
	})
}

// 新增快递订单信息
export function addKdOrderInfo(data) {
	return request({
		url: '/biz/kdOrderInfo',
		method: 'post',
		data: data
	})
}

// 修改快递订单信息
export function updateKdOrderInfo(data) {
	return request({
		url: '/biz/kdOrderInfo',
		method: 'put',
		data: data
	})
}

// 删除快递订单信息
export function delKdOrderInfo(id) {
	return request({
		url: '/biz/kdOrderInfo/' + id,
		method: 'delete'
	})
}
// 取消快递订单
export function cancelKdOrder(id, data) {
	return request({
		url: '/biz/kdOrderInfo/cancelKdOrder/' + id,
		method: 'delete',
		data: data
	})
}