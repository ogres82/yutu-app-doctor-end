import request from '@/utils/request'

// 查询首页轮播广告列表
export function listAdvertise(query) {
  return request({
    url: '/biz/advertise/list',
    method: 'get',
    params: query
  })
}

// 查询首页轮播广告详细
export function getAdvertise(id) {
  return request({
    url: '/biz/advertise/' + id,
    method: 'get'
  })
}

// 新增首页轮播广告
export function addAdvertise(data) {
  return request({
    url: '/biz/advertise',
    method: 'post',
    data: data
  })
}

// 修改首页轮播广告
export function updateAdvertise(data) {
  return request({
    url: '/biz/advertise',
    method: 'put',
    data: data
  })
}

// 删除首页轮播广告
export function delAdvertise(id) {
  return request({
    url: '/biz/advertise/' + id,
    method: 'delete'
  })
}
