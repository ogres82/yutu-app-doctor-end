import request from '@/utils/request'

// 逆地理编码
export function geocodeRegeo(longitude, latitude) {
	return request({
		url: '/biz/amap/geocode/regeo/' + longitude + '/' + latitude,
		method: 'get'
	})
}