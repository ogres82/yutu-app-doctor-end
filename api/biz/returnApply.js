import request from '@/utils/request'
export function addOmsOrderReturnApply(data) {
	return request({
		url: '/biz/returnApply',
		method: 'post',
		data: data
	})
}