import request from '@/utils/request'

// 查询高德城市列表
export function listGdCity(query) {
  return request({
    url: '/biz/gdCity/list',
    method: 'get',
    params: query
  })
}

// 查询高德城市详细
export function getGdCity(id) {
  return request({
    url: '/biz/gdCity/' + id,
    method: 'get'
  })
}

// 新增高德城市
export function addGdCity(data) {
  return request({
    url: '/biz/gdCity',
    method: 'post',
    data: data
  })
}

// 修改高德城市
export function updateGdCity(data) {
  return request({
    url: '/biz/gdCity',
    method: 'put',
    data: data
  })
}

// 删除高德城市
export function delGdCity(id) {
  return request({
    url: '/biz/gdCity/' + id,
    method: 'delete'
  })
}
// 查询高德城市列表
export function getCityInfoByCityCode(query) {
  return request({
    url: '/biz/gdCity/getCityInfoByCityCode',
    method: 'get',
    params: query
  })
}

