import request from '@/utils/request'

// 查询系统医院列表
export function listSysHospitalOrganization(query) {
	return request({
		url: '/biz/sysHospitalOrganization/list',
		method: 'get',
		params: query
	})
}

// 查询系统医院详细
export function getSysHospitalOrganization(orgId) {
	return request({
		url: '/biz/sysHospitalOrganization/' + orgId,
		method: 'get'
	})
}

// 新增系统医院
export function addSysHospitalOrganization(data) {
	return request({
		url: '/biz/sysHospitalOrganization',
		method: 'post',
		data: data
	})
}

// 修改系统医院
export function updateSysHospitalOrganization(data) {
	return request({
		url: '/biz/sysHospitalOrganization',
		method: 'put',
		data: data
	})
}
 
// 删除系统医院
export function delSysHospitalOrganization(orgId) {
	return request({
		url: '/biz/sysHospitalOrganization/' + orgId,
		method: 'delete'
	})
}


// 查询系统医院详细
export function selectHospitalByAreaId(countyId) {
	return request({
		url: '/biz/sysHospitalOrganization/selectHospitalByAreaId/' + countyId,
		method: 'get'
	})
}


// 查询一级科室或二级科室信息
export function selectDepartLists(query) {
	return request({
		url: '/biz/sysHospitalOrganization/selectDepartLists',
		method: 'get',
		params: query
	})
}


// 查询系统医院详细
// export function (query) {
// 	return request({
// 		url: '/biz/sysHospitalOrganization/selectDepartLists",
// 		method: 'get',
// 		params: query
// 	})
// }