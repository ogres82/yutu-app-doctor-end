import request from '@/utils/request'

// 查询商品信息列表
export function listPmsProductInfo(query) {
	return request({
		url: '/biz/pmsProductInfo/list',
		method: 'get',
		params: query
	})
}

// 查询商品信息详细
export function getPmsProductInfo(productId) {
	return request({
		url: '/biz/pmsProductInfo/' + productId,
		method: 'get'
	})
}

// 新增商品信息
export function addPmsProductInfo(data) {
	return request({
		url: '/biz/pmsProductInfo',
		method: 'post',
		data: data
	})
}

// 修改商品信息
export function updatePmsProductInfo(data) {
	return request({
		url: '/biz/pmsProductInfo',
		method: 'put',
		data: data
	})
}

// 删除商品信息
export function delPmsProductInfo(productId) {
	return request({
		url: '/biz/pmsProductInfo/' + productId,
		method: 'delete'
	})
}


// 查询商品信息列表
export function selectByColumnPage(query) {
	return request({
		url: '/biz/pmsProductInfo/selectByColumnPage',
		method: 'get',
		params: query
	})
}
// 查询项目或者商铺
export function selectProductOrInstitutions(query) {
	return request({
		url: '/biz/pmsProductInfo/selectProductOrInstitutions',
		method: 'get',
		params: query
	})
}