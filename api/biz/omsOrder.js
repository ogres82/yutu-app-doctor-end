import request from '@/utils/request'

// 查询订单列表
export function listOmsOrder(query) {
	return request({
		url: '/biz/omsOrder/list',
		method: 'get',
		params: query
	})
}

// 查询订单详细
export function getOmsOrder(orderId) {
	return request({
		url: '/biz/omsOrder/' + orderId,
		method: 'get'
	})
}

// 新增订单
export function addOmsOrder(data) {
	return request({
		url: '/biz/omsOrder',
		method: 'post',
		data: data
	})
}

// 修改订单
export function updateOmsOrder(data) {
	return request({
		url: '/biz/omsOrder',
		method: 'put',
		data: data
	})
}

// 删除订单
export function delOmsOrder(orderId) {
	return request({
		url: '/biz/omsOrder/' + orderId,
		method: 'delete'
	})
}

// 查询”我的“订单各个状态总数：传入用户id
export function selectMyOrderCountByStatus(userId) {
	return request({
		url: '/biz/omsOrder/selectMyOrderCountByStatus/' + userId,
		method: 'get',
	})
}

// 查询推广订单
export function extendList(query) {
	return request({
		url: '/biz/omsOrder/extendList',
		method: 'get',
		data: query
	})
}

// 查询推广订单
export function statExtendList(query) {
	return request({
		url: '/biz/omsOrder/statExtendList',
		method: 'get',
		data: query
	})
}
// 查询订单列表
export function selectMyOrderListByStatus(userId, query) {
	return request({
		url: '/biz/omsOrder/selectMyOrderListByStatus/' + userId,
		method: 'get',
		params: query
	})
}

// 修改订单备注
export function updateOmsOrderNote(data) {
	return request({
		url: '/biz/omsOrder/updateOmsOrderNote',
		method: 'put',
		data: data
	})
}
// 修改订单备注
export function selectByOrderSn(orderSn) {
	return request({
		url: '/biz/omsOrder/selectByOrderSn/' + orderSn,
		method: 'get',
		// params: query
	})
}