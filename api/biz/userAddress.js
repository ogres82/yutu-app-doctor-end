import request from '@/utils/request'

// 查询收发货地址列表
export function listUserAddress(query) {
	return request({
		url: '/biz/userAddress/list',
		method: 'get',
		params: query
	})
}

// 查询收发货地址详细
export function getUserAddress(addressId) {
	return request({
		url: '/biz/userAddress/' + addressId,
		method: 'get'
	})
}

// 新增收发货地址
export function addUserAddress(data) {
	return request({
		url: '/biz/userAddress',
		method: 'post',
		data: data
	})
}

// 修改收发货地址
export function updateUserAddress(data) {
	return request({
		url: '/biz/userAddress',
		method: 'put',
		data: data
	})
}

// 删除收发货地址
export function delUserAddress(addressId) {
	return request({
		url: '/biz/userAddress/' + addressId,
		method: 'delete'
	})
}
// 新增或更新地址
export function addOrUpOmsAddress(data) {
	return request({
		url: '/biz/userAddress/addOrUpOmsAddress',
		method: 'post',
		data: data
	})
}