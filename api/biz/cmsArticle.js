import request from '@/utils/request'

// 查询资讯信息列表
export function listCmsArticle(query) {
	return request({
		url: '/biz/cmsArticle/list',
		method: 'get',
		params: query
	})
}

// 查询资讯信息详细
export function getCmsArticle(articleId) {
	return request({
		url: '/biz/cmsArticle/' + articleId,
		method: 'get'
	})
}

// 新增资讯信息
export function addCmsArticle(data) {
	return request({
		url: '/biz/cmsArticle',
		method: 'post',
		data: data
	})
}

// 修改资讯信息
export function updateCmsArticle(data) {
	return request({
		url: '/biz/cmsArticle',
		method: 'put',
		data: data
	})
}

// 修改资讯信息
export function saveOrSubmit(data) {
	return request({
		url: '/biz/cmsArticle/saveOrSubmit',
		method: 'put',
		data: data
	})
}

// 删除资讯信息
export function delCmsArticle(articleId) {
	return request({
		url: '/biz/cmsArticle/' + articleId,
		method: 'delete'
	})
}

// selectByColumnPage
export function selectByColumnPage(query) {
	return request({
		url: '/biz/cmsArticle/selectByColumnPage',
		method: 'get',
		params: query
	})
}