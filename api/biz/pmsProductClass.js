import request from '@/utils/request'

// 查询商品分类列表
export function listPmsProductClass(query) {
  return request({
    url: '/biz/pmsProductClass/list',
    method: 'get',
    params: query
  })
}

// 查询商品分类详细
export function getPmsProductClass(classId) {
  return request({
    url: '/biz/pmsProductClass/' + classId,
    method: 'get'
  })
}

// 新增商品分类
export function addPmsProductClass(data) {
  return request({
    url: '/biz/pmsProductClass',
    method: 'post',
    data: data
  })
}

// 修改商品分类
export function updatePmsProductClass(data) {
  return request({
    url: '/biz/pmsProductClass',
    method: 'put',
    data: data
  })
}

// 删除商品分类
export function delPmsProductClass(classId) {
  return request({
    url: '/biz/pmsProductClass/' + classId,
    method: 'delete'
  })
}
// 查询商品分类列表
export function selectClassPathByColumn(query) {
  return request({
    url: '/biz/pmsProductClass/selectClassPathByColumn',
    method: 'get',
    params: query
  })
}
