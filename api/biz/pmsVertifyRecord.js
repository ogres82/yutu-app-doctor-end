import request from '@/utils/request'

// 查询审核记录
export function listPmsVertifyRecord(query) {
	return request({
		url: '/biz/VertifyRecord/list',
		method: 'get',
		params: query
	})
}
