import request from '@/utils/request'

// 查询检测人员信息列表
export function listSubjectInfo(query) {
  return request({
    url: '/biz/subjectInfo/list',
    method: 'get',
    params: query
  })
}

// 查询检测人员信息详细
export function getSubjectInfo(omsUserId) {
  return request({
    url: '/biz/subjectInfo/' + omsUserId,
    method: 'get'
  })
}

// 新增检测人员信息
export function addSubjectInfo(data) {
  return request({
    url: '/biz/subjectInfo',
    method: 'post',
    data: data
  })
}

// 修改检测人员信息
export function updateSubjectInfo(data) {
  return request({
    url: '/biz/subjectInfo',
    method: 'put',
    data: data
  })
}

// 删除检测人员信息
export function delSubjectInfo(omsUserId) {
  return request({
    url: '/biz/subjectInfo/' + omsUserId,
    method: 'delete'
  })
}
