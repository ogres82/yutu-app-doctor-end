import request from '@/utils/request'

// 查询提示说明列表
export function listTips(query) {
  return request({
    url: '/biz/tips/list',
    method: 'get',
    params: query
  })
}

// 查询提示说明详细
export function getTips(id) {
  return request({
    url: '/biz/tips/' + id,
    method: 'get'
  })
}

// 新增提示说明
export function addTips(data) {
  return request({
    url: '/biz/tips',
    method: 'post',
    data: data
  })
}

// 修改提示说明
export function updateTips(data) {
  return request({
    url: '/biz/tips',
    method: 'put',
    data: data
  })
}

// 删除提示说明
export function delTips(id) {
  return request({
    url: '/biz/tips/' + id,
    method: 'delete'
  })
}
