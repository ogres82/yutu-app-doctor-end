import request from '@/utils/request'

// 查询用户申请记录列表
export function listRecord(query) {
	return request({
		url: '/biz/record/list',
		method: 'get',
		params: query
	})
}

// 查询用户申请记录详细
export function getRecord(userId) {
	return request({
		url: '/biz/record/' + userId,
		method: 'get'
	})
}

// 新增用户申请记录
export function addRecord(data) {
	return request({
		url: '/biz/record',
		method: 'post',
		data: data
	})
}

// 修改用户申请记录
export function updateRecord(data) {
	return request({
		url: '/biz/record',
		method: 'put',
		data: data
	})
}

// 删除用户申请记录
export function delRecord(userId) {
	return request({
		url: '/biz/record/' + userId,
		method: 'delete'
	})
}

// 获取用户下的推广团队信息（app）
export function getWorkerList(userId) {
	return request({
		url: '/biz/record/workerList/' + userId,
		method: 'get'
	})
}


// 获取分享码和分享码二维码
export function shareQrCode() {
	return request({
		url: '/biz/record/qrCode',
		method: 'get'
	})
}