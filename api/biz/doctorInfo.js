import request from '@/utils/request'

// 查询医生信息列表
export function listDoctorInfo(query) {
	return request({
		url: '/biz/doctorInfo/list',
		method: 'get',
		params: query
	})
}

// 查询医生信息详细
export function getDoctorInfo(doctorId) {
	return request({
		url: '/biz/doctorInfo/' + doctorId,
		method: 'get'
	})
}

// 新增医生信息
export function addDoctorInfo(data) {
	return request({
		url: '/biz/doctorInfo',
		method: 'post',
		data: data
	})
}

// 修改医生信息
export function updateDoctorInfo(data) {
	return request({
		url: '/biz/doctorInfo',
		method: 'put',
		data: data
	})
}

// 删除医生信息
export function delDoctorInfo(doctorId) {
	return request({
		url: '/biz/doctorInfo/' + doctorId,
		method: 'delete'
	})
}
// 医生或护士认证
export function authentication(data) {
	return request({
		url: '/biz/doctorInfo/authentication',
		method: 'post',
		data: data
	})
}

// 通过用户Id找到医生或护士信息
export function selectDoctOrNurseByUserId(userId) {
	return request({
		url: '/biz/doctorInfo/selectDoctOrNurseByUserId/' + userId,
		method: 'get'
	})
}