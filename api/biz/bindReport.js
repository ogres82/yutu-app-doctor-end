import request from '@/utils/request'

// 查询提示说明列表
export function listReport(query) {
	return request({
		url: '/biz/bindReport/list',
		method: 'get',
		params: query
	})
}

// 查询提示说明详细
export function getReport(bindId) {
	return request({
		url: '/biz/bindReport/' + bindId,
		method: 'get'
	})
}

// 新增提示说明
export function addReport(data) {
	return request({
		url: '/biz/bindReport',
		method: 'post',
		data: data
	})
}

// 修改提示说明
export function updateReport(data) {
	return request({
		url: '/biz/bindReport',
		method: 'put',
		data: data
	})
}

// 删除提示说明
export function delReport(bindId) {
	return request({
		url: '/biz/bindReport/' + bindId,
		method: 'delete'
	})
}

// 查询没有 被绑定报告
export function noBindListReport(query) {
	return request({
		url: '/biz/bindReport/noBind/list',
		method: 'get',
		params: query
	})
}


// 报告绑定
export function bindReport(data) {
	return request({
		url: '/biz/bindReport/bind',
		method: 'post',
		data: data
	})
}

// 查询一个医生的关联了的报告
export function selectDocReport(query) {
	return request({
		url: '/biz/bindReport/doc/report',
		method: 'get',
		params: query
	})
}