import request from '@/utils/request'

// 查询未派工单列表
export function list() {
	return request({
		url: '/biz/takeOrders/list',
		method: 'get',
	})
}

// 护士已接工单列表
export function myLists(nurseId) {
	return request({
		url: '/biz/takeOrders/myLists/' + nurseId,
		method: 'get'
	})
}

// 护士接单
export function take(data) {
	return request({
		url: '/biz/takeOrders/take',
		method: 'put',
		data: data
	})
}

// 护士取消接单
export function unTake(orderItemId) {
	return request({
		url: '/biz/takeOrders/unTake/' + orderItemId,
		method: 'put',
	})
}

// 护士完成订单
export function finishOrder(orderItemId) {
	return request({
		url: '/biz/takeOrders/finishOrder/' + orderItemId,
		method: 'put'
	})
}

// 护士完成送检
export function sendOrder(orderItemId) {
	return request({
		url: '/biz/takeOrders/sendOrder',
		method: 'put',
		data: orderItemId
	})
}