import request from '@/utils/request'
import ossUpload from '@/utils/ossUpload'
// 上传一个文件
export function ossUploadOne(data) {
	return ossUpload({
		url: '/biz/NurseInfo/oss/upload',
		filePath: data
	})
}
// 上传多个文件
export function ossUploadMulti(data) {
	console.log("ossUploadMulti",data);
	return ossUpload({
		url: '/biz/NurseInfo/oss/upload',
		files: data
	})
}
// 删除一个文件
export function ossDelete(data) {
	return request({
		url: '/biz/NurseInfo/oss/delete',
		method: 'post',
		data: data
	})
}