import request from '@/utils/request'

// 查询用户产品收藏列表
export function listFavorite(query) {
	return request({
		url: '/biz/favorite/list',
		method: 'get',
		params: query
	})
}

// 查询用户产品收藏详细
export function getFavorite(userId) {
	return request({
		url: '/biz/favorite/' + userId,
		method: 'get'
	})
}

// 新增用户产品收藏
export function addFavorite(data) {
	return request({
		url: '/biz/favorite',
		method: 'post',
		data: data
	})
}

// 修改用户产品收藏
export function updateFavorite(data) {
	return request({
		url: '/biz/favorite',
		method: 'put',
		data: data
	})
}

// 删除用户产品收藏
export function delFavorite(userId) {
	return request({
		url: '/biz/favorite/' + userId,
		method: 'delete'
	})
}

// 删除用户产品收藏
export function delOneFavorite(data) {
	return request({
		url: '/biz/favorite/one',
		method: 'delete',
		data: data
	})
}