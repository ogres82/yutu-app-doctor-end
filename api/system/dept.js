import request from '@/utils/request'
// 查询部门列表
export function listDept(query) {
	return request({
		url: '/system/dept/list',
		method: 'get',
		params: query
	})
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
	return request({
		url: '/system/dept/list/exclude/' + deptId,
		method: 'get'
	})
}

// 查询部门详细
export function getDept(deptId) {
	return request({
		url: '/system/dept/' + deptId,
		method: 'get'
	})
}

// 查询部门下拉树结构
export function treeselect(query) {
	return request({
		url: '/system/dept/treeselect',
		method: 'get',
		params: query
	})
}

// 查询部门下拉树结构
export function treeselectByDept(deptId) {
	return request({
		url: '/system/dept/treeselect/' + deptId,
		method: 'get'
	})
}

// 根据角色ID查询部门树结构
export function roleDeptTreeselect(roleId) {
	return request({
		url: '/system/dept/roleDeptTreeselect/' + roleId,
		method: 'get'
	})
}
// 查询部门列表
export function listDeptByType(query) {
	return request({
		url: '/system/dept/listByType',
		method: 'get',
		params: query
	})
}
// app获取部门列表
export function selectByRegionId(query) {
	return request({
		url: '/system/dept/selectByRegionId',
		method: 'get',
		params: query
	})
}