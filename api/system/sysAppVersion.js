import request from '@/utils/request'

// 查询基础字段列表
export function listSysAppVersion(query) {
	return request({
		url: '/system/sysAppVersion/list',
		method: 'get',
		params: query
	})
}

// 查询基础字段详细
export function getSysAppVersion(id) {
	return request({
		url: '/system/sysAppVersion/' + id,
		method: 'get'
	})
}

// 新增基础字段
export function addSysAppVersion(data) {
	return request({
		url: '/system/sysAppVersion',
		method: 'post',
		data: data
	})
}

// 修改基础字段
export function updateSysAppVersion(data) {
	return request({
		url: '/system/sysAppVersion',
		method: 'put',
		data: data
	})
}

// 删除基础字段
export function delSysAppVersion(id) {
	return request({
		url: '/system/sysAppVersion/' + id,
		method: 'delete'
	})
}


// 通过AppKey获取最新的版本信息
export function getLastByAppKey(appKey) {
	return request({
		url: '/system/sysAppVersion/latest/' + appKey,
		method: 'get',
	})
}
