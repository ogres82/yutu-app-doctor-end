import upload from '@/utils/upload'
import request from '@/utils/request'

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
	const data = {
		oldPassword,
		newPassword
	}
	return request({
		url: '/system/user/profile/updatePwd',
		method: 'put',
		params: data
	})
}

// 查询用户个人信息
export function getUserProfile() {
	return request({
		url: '/system/user/profile',
		method: 'get'
	})
}

// 修改用户个人信息
export function updateUserProfile(data) {
	return request({
		url: '/system/user/profile',
		method: 'put',
		data: data
	})
}

// 用户头像上传
export function uploadAvatar(data) {
	return upload({
		url: '/system/user/profile/avatar',
		name: data.name,
		filePath: data.filePath
	})
}
// 通过OSS方式更新头像
export function uploadAvatarOss(avatar) {
	return request({
		url: '/system/user/profile/avatar/oss',
		method: 'put',
		params: {
			avatar
		}
	})
}
// 查询用户列表
export function listUser(query) {
	return request({
		url: '/system/user/list',
		method: 'get',
		params: query
	})
}
// 查询用户列表-不通过权限范围
export function listUserNoScope(query) {
	return request({
		url: '/system/user/listNoScope',
		method: 'get',
		params: query
	})
}


//  注销一个账号
export function stopUser() {
	return request({
		url: '/system/user/stopUser',
		method: 'put',
	})
}

// 用户密码重置
export function retrievePassword(data) {
	return request({
		url: '/system/user/profile/retrievePassword',
		method: 'put',
		params: data
	})
}
// yon
export function updateUserMerchantId(data) {
	return request({
		url: '/system/user/updatAlipayByUserId',
		method: 'put',
		params: data
	})
}


// 根据用户编号获取详细信息
export function getUserInfoById(userId) {
	return request({
		url: '/system/user/'+userId,
		method: 'get'
	})
}


