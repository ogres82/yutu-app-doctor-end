import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
	return request({
		url: '/system/role/list',
		method: 'get',
		params: query
	})
}

// 查询角色详细
export function getRole(roleId) {
	return request({
		url: '/system/role/' + roleId,
		method: 'get'
	})
}

// 角色数据权限
export function dataScope(data) {
	return request({
		url: '/system/role/dataScope',
		method: 'put',
		data: data
	})
}

// 查询角色已授权用户列表
export function allocatedUserList(query) {
	return request({
		url: '/system/role/authUser/allocatedList',
		method: 'get',
		params: query
	})
}

// 查询角色已授权用户列表
export function allocatedListByRole(query) {
	return request({
		url: '/system/role/authUser/allocatedListByRole',
		method: 'get',
		params: query
	})
}

// 查询角色未授权用户列表
export function unallocatedUserList(query) {
	return request({
		url: '/system/role/authUser/unallocatedList',
		method: 'get',
		params: query
	})
}

// 授权用户选择
export function authUserSelectAll(data) {
	return request({
		url: '/system/role/authUser/selectAll',
		method: 'put',
		params: data
	})
}
