import request from '@/utils/request'
import upload from '@/utils/upload'

// 查询文件信息列表
export function listFileInfo(query) {
	return request({
		url: '/system/fileInfo/list',
		method: 'get',
		params: query
	})
}

// 查询文件信息详细
export function getFileInfo(fileId) {
	return request({
		url: '/system/fileInfo/' + fileId,
		method: 'get'
	})
}

// 新增文件信息
export function addFileInfo(data) {
	return request({
		url: '/system/fileInfo',
		method: 'post',
		data: data
	})
}

// 修改文件信息
export function updateFileInfo(data) {
	return request({
		url: '/system/fileInfo',
		method: 'put',
		data: data
	})
}

// 删除文件信息
export function delFileInfo(fileId) {
	return request({
		url: '/system/fileInfo/' + fileId,
		method: 'delete'
	})
}

// 文件上传
export function uploadFile(data) {
	return upload({
		url: '/common/upload',
		filePath: data
	})
}

// 查询文件信息列表
export function listFileInfoByBiz(bizId, bizType, bizSubType) {
	let query = {
		bizId,
		bizType,
		bizSubType,
		pageSize: 1000
	}
	return request({
		url: '/system/fileInfo/list',
		method: 'get',
		params: query
	})
}
