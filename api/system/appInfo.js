import request from '@/utils/request'
// 查询App信息
export function getAppInfo() {
	return request({
		url: '/system/config/HeNiuApp',
		method: 'get'
	})
}
