import request from '@/utils/request'
import constant from "@/utils/constant";

// 登录方法
export function login(username, password, code, uuid) {
	const data = {
		username,
		password,
		code,
		uuid
	}
	return request({
		'url': '/login',
		headers: {
			isToken: false
		},
		'method': 'post',
		'data': data
	})
}

// 获取用户详细信息
export function getInfo() {
	return request({
		'url': '/getInfo',
		'method': 'get'
	})
}

// 退出方法
export function logout() {
	return request({
		'url': '/logout',
		'method': 'post'
	})
}

// 获取验证码
export function getCodeImg() {
	return request({
		'url': '/captchaImage',
		headers: {
			isToken: false
		},
		method: 'get',
		timeout: 20000
	})
}
//获取app版本信息
export function getAppVersion() {
	return request({
		url: '/f500AppConfig/getNewVersion',
		method: 'get',
		header: {
			getVersionUUid: constant.externalUUid
		}
	})
}

//获取短信验证码
export function getSmsCaptcha(phone) {
	return request({
		url: '/captcha/sms/register/' + phone,
		method: 'get'
	})
}

//注册信息K
export function register(data) {
	return request({
		url: '/register',
		method: 'post',
		data: data
	})
}

//注册信息K
export function checkToken() {
	return request({
		url: '/checkToken',
		method: 'get'
	})
}