import {
	ossUploadMulti,
	ossUploadOne
} from '@/api/biz/ossFile'
import permision from "@/common/permission.js"
import androidPermissionCheck from "@/utils/androidPermissionCheck.js"
import config from '@/config'
import {
	getToken
} from '@/utils/auth'

export default {
	/**
	 * 选择并上传图片
	 */
	chooseImageAndUpload(count) {
		console.log("permision.isIOS",permision.isIOS);
		if (!permision.isIOS) {
			console.log("chooseImageAndUpload===11111111");
			return androidPermissionCheck.promiseCheck("CAMERA")
				.then(res => {
					if (res) {
						return this.chooseAndUpload(count)
					} else {
						return new Promise((resolve, reject) => {
							reject("没有授权，上传失败")
						})
					}
				})
		} else {
			return this.chooseAndUpload()
		}
	},
	chooseAndUpload(count) {
		console.log("chooseAndUpload--count",count);
		return new Promise((resolve, reject) => {
			uni.chooseImage({
				count: count || 1, //默认9
				sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
				// sourceType: ['album'], //从相册选择
				success: (res) => {
					if (res.tempFilePaths.length > 1) {
						let tempFiles = res.tempFiles
						// for (let tempfile of tempFiles) {
						// 	tempfile.uri=tempfile.path
						// }
						ossUploadMulti(tempFiles)
							.then(res => {
								resolve(res)
							})
							.catch(err => {
								reject(err)
							})
					} else {
						ossUploadOne(res.tempFilePaths[0])
							.then(res => {
								resolve(res)
							})
							.catch(err => {
								reject(err)
							})
					}
				},
				fail: err => {
					reject("选择文件错误")
				}
			});
		})
	},


	/**
	 * 扫描图片
	 */
	scanImage() {
		if (!permision.isIOS) {
			return androidPermissionCheck.promiseCheck("CAMERA")
				.then(res => {
					if (res) {
						return this.scanCode()
					} else {
						return new Promise((resolve, reject) => {
							reject("没有授权，上传失败")
						})
					}
				})
		} else {
			return this.scanCode()
		}
	},
	/**
	 * 实际扫描代码
	 */
	scanCode() { //真正的对二维码进行扫描
		return new Promise((resolve, reject) => {
			uni.scanCode({
				success: (res) => {
					if (res.result) {
						resolve(res)
					} else {
						reject("扫码失败，请重新扫码")
					}
				}
			});
		})
	},
	/**
	 * 打电话
	 */
	callPhone(phone) {
		console.log("callPhone", phone);
		if (!phone) {
			this.$madal.msg("拨打电话不能为空！")
			return
		}
		if (!permision.isIOS) {
			return androidPermissionCheck.promiseCheck("CALL_PHONE")
				.then(res => {
					if (res) {
						console.log("phone,");
						return this.makePhoneCall(phone)
					} else {
						return new Promise((resolve, reject) => {
							reject("没有授权，拨打失败")
						})
					}
				})
		} else {
			return this.makePhoneCall()
		}
	},
	makePhoneCall(phone) {
		uni.makePhoneCall({
			phoneNumber: phone,
			success: (res) => {
				console.log("makePhoneCall===success", res);
			},
			fail: (err) => {
				console.log("makePhoneCall===fail", err);
			}
		});
	}
}