import modal from '@/plugins/modal.js'
export function downFilePromise(fileUrl) { //下载一个文件
	return new Promise((resolve, reject) => {
		uni.downloadFile({
			url: fileUrl,
			success: down => {
				if (down.statusCode === 200) {
					resolve(down.tempFilePath)
				} else {
					reject("下载失败")
				}
			},
			fail: () => {
				reject("下载失败")
			}
		})
	})
}


export function barcodeScanPromise(imgFilePath) { //识别本地图片二维码
	return new Promise((resolve, reject) => {
		plus.barcode.scan(imgFilePath,
			(type, res) => { //扫码识别成功回调函数
				resolve(res)
			},
			error => { //扫码识别失败回调函数
				reject(error.message)
			})
	})
}
export function sharImagePromise(imgFilePath) { //微信分享图片
	return new Promise((resolve, reject) => {
		uni.share({
			provider: "weixin",
			scene: "WXSceneSession",
			type: 2,
			imageUrl: imgFilePath,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		});
	})
}
export function sharServiceChatPromise(serverUrl) { //分享企业服务
	// plus.runtime.openWeb(serverUrl);
	// plus.runtime.openURL(serverUrl);
	console.log("”serverUrl", serverUrl);
	let newUrl = serverUrl
	// newUrl="weixin://dl/business/?ticket=kfc9fb17b0e9a7f0705"
	console.log("”newUrl", newUrl);
	plus.runtime.openURL(newUrl);

	// return new Promise((resolve, reject) => {
	// 	uni.share({
	// 		provider: "weixin",
	// 		openCustomerServiceChat: true,
	// 		customerUrl: serverUrl, //企业微信地址
	// 		// corpid: 'ww13edaa**********', //企业id
	// 		success: (res) => {
	// 			console.log("sharServiceChatPromise==成功", res);
	// 			resolve(res)
	// 		},
	// 		fail: (err) => {
	// 			console.log("sharServiceChatPromise==失败", err);
	// 			reject(err.errMsg)
	// 		}
	// 	});
	// })
}

export function previewLinkImage(linkImg) { //显示联系我们的图片
	if (!linkImg) {
		return $modal.msg("商家没有联系方式！")
		return
	}
	uni.previewImage({
		urls: [linkImg],
		longPressActions: {
			itemList: ['微信分享', '识别二维码'],
			success: (res) => {
				if (res.tapIndex == 0) { //微信分享
					modal.loading()
					downFilePromise(linkImg)
						.then(res => {
							return sharImagePromise(res)
						})
						.then(res => {
							modal.closeLoading()
						})
						.catch(err => {
							modal.closeLoading()
							modal.msg(err)
						})
				} else {
					modal.loading()
					downFilePromise(linkImg)
						.then(res => {
							return barcodeScanPromise(res)
						})
						.then(res => {
							modal.closeLoading()
							sharServiceChatPromise(res)
						})
						.catch(err => {
							modal.closeLoading()
							console.log("err”", err);
							modal.msg(err)
						})
				}
			},
			fail: (res) => {
				console.log(res.errMsg);
			}
		},
	})
}