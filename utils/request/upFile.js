let baseUrl = 'http://192.168.43.228:18080';
let upFile = {};
let methods = ['post']
for (let method of methods) {
	upFile[method] = function(api, filePath, fileName, data, callback) {
		if (typeof(data) == 'function') {
			callback = data
			data = {}
		}
		const value = uni.getStorageSync('baseUrl');
		if (value) {
			baseUrl = value;
		}
		data.token = 'test';
		data.appId = 'test';
		uni.uploadFile({
			url: baseUrl + api, //仅为示例，非真实的接口地址
			filePath: filePath,
			name: fileName,
			formData: data,
			success: function(res) {
				callback(res);
			},
			fail: function(err) {
				//调取测试服务器接口,判断是否是服务器地址问题
				uni.request({
					url: baseUrl + '/Inspection/api/v1/test/testRequestWeb',
					method: 'GET',
					fail: function(err) {
						//跳转到服务器地址设置
						uni.navigateTo({
							url: '/pages/request/request'
						});
					}
				});
			}
		})
	}
}

export default upFile
