let distinguish = {};
let methods = ['post']
for (let method of methods) {
	distinguish[method] = function(Bsuccess, Berror) {
		//获取识别Token
		uni.request({
			url: 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=UNGgolC1WHS4x6gTYh5GMEbg&client_secret=5dNXVFYCTIFmNRcGQLEwQ7q4oSDXFvZU',
			method: "GET",
			success: function(resAPI) {
				uni.setStorageSync('access_token', resAPI.data
					.access_token);
			},
		})
		//图片识别
		uni.request({
			url: 'https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic?access_token=' +
				uni.getStorageSync('access_token'),
			data: {
				image: picUrl,
				language_type: 'CHN_ENG', //识别语言类型，默认中英文结合
				detect_direction: true, //检测图像朝向
			},
			method: 'POST',
			header: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			success: (res) => {
				Bsuccess(res);
			},
			fail(err) {
				Berror(err);
			}
		})
	}
}
export default distinguish
