let baseUrl = 'https://api.lfonehealth.com1111111';
let baseUrlWC = 'wss://api.lfonehealth.com/ws11111';

uni.setStorageSync('baseUrl', baseUrl)
uni.setStorageSync('baseUrlWC', baseUrlWC)


let ajax = {};
let methods = ['get', 'post', 'put', 'delete']
for (let method of methods) {
	ajax[method] = function(api, data, callback) {
		if (typeof(data) == 'function') {
			callback = data
			data = {}
		}
		uni.request({
			url: baseUrl + api,
			data: data,
			method: method,
			header: {
				'Content-type': 'application/x-www-form-urlencoded',
			},
			dataType: 'json',
			responseType: 'text',
			success: function(res) {
				callback(res.data)
			},
			fail: function(err) {
				uni.showToast({
					title: '请检查网络是否连接，或本机是否同意接入网络连接',
					mask: false,
					duration: 5000,
					icon: "none"
				});
				console.log(api,JSON.stringify(err))
				//调取测试服务器接口,判断是否是服务器地址问题
			},
		})
	}
}

export default ajax
