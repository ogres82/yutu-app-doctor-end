// 通过条码扫描结果获取到推荐人的Id
export function getRecommendIdByQrCodeResult(result) {
	let recommendId = this.getRecommendCodeByQrCodeResult(result)
	return parseInt(recommendId)
}

// 通过条码扫描结果获取到推荐人的Id
export function getRecommendCodeByQrCodeResult(result) {
	//code=000058&time=1715431898062
	if (!result) { //避免空报错
		result = "";
	}
	let startIndex = result.indexOf("=")
	let endIndex = result.indexOf("&")
	return result.substring(startIndex + 1, endIndex)
}
//验证手机号
export function checkMobile(value) {
	const regMobile = /^[1][3,4,5,7,8,9][0-9]{9}$/;
	if (regMobile.test(value)) {
		// 合法的手机号码
		return true;
	}
	uni.showToast({
		title: '手机号码不合法',
		mask: false,
		duration: 1000,
		icon: "none"
	});
	return false;
}

//身份证校验
export function sfzjy(value) {
	if (!/(^\d{15}$)|(^\d{17}(\d|X|x)$)/.test(value)) {
		uni.showToast({
			title: '你输入的身份证长度或格式错误',
			mask: false,
			duration: 1000,
			icon: "none"
		});
		return false
	}
	// 身份证城市
	const aCity = {
		11: '北京',
		12: '天津',
		13: '河北',
		14: '山西',
		15: '内蒙古',
		21: '辽宁',
		22: '吉林',
		23: '黑龙江',
		31: '上海',
		32: '江苏',
		33: '浙江',
		34: '安徽',
		35: '福建',
		36: '江西',
		37: '山东',
		41: '河南',
		42: '湖北',
		43: '湖南',
		44: '广东',
		45: '广西',
		46: '海南',
		50: '重庆',
		51: '四川',
		52: '贵州',
		53: '云南',
		54: '西藏',
		61: '陕西',
		62: '甘肃',
		63: '青海',
		64: '宁夏',
		65: '新疆',
		71: '台湾',
		81: '香港',
		82: '澳门',
		91: '国外',
	}
	if (!aCity[parseInt(value.substr(0, 2))]) {
		uni.showToast({
			title: '你的身份证地区非法',
			mask: false,
			duration: 1000,
			icon: "none"
		});
		return false
	}
	// 出生日期验证
	const sBirthday = (value.substr(6, 4) + '-' + Number(value.substr(10, 2)) + '-' + Number(value.substr(12,
		2))).replace(
		/-/g,
		'/'
	)
	const d = new Date(sBirthday)
	if (sBirthday !== d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate()) {
		uni.showToast({
			title: '身份证上的出生日期非法',
			mask: false,
			duration: 1000,
			icon: "none"
		});
		return false
	}

	// 身份证号码校验
	let sum = 0
	const weights = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
	const codes = '10X98765432'
	for (let i = 0; i < value.length - 1; i++) {
		sum += value[i] * weights[i]
	}
	const last = codes[sum % 11] // 计算出来的最后一位身份证号码
	if (value[value.length - 1] !== last) {
		uni.showToast({
			title: '你输入的身份证号非法',
			mask: false,
			duration: 1000,
			icon: "none"
		});
		return false
	}
	return true
}