import store from "../../store/index.js"

export function localGet(key) {
	return store.state[key];
}

export function localSet(key, value) {
	store.commit(key, value)
}

export function checkLogin() {
	let userInfo = JSON.stringify(localGet('userInfo').userInfo)
	if (userInfo == undefined || userInfo == null || userInfo == '') {
		return false;
	} else {
		return true;
	}
}

export function checkAndToLogin() {
	if (!checkLogin()) {
		uni.navigateTo({
			url: '../login/login'
		});
	}
}

export function localFileGet(key) {
	let resData = ''
	uni.getStorage({
		key: key,
		success: function(res) {
			resData = res.data
		}
	});
	return resData;
}

export function localFileSet(key, value) {
	uni.setStorage({
		key: key,
		data: value,
	});
}

export function localFileRemove(key) {
	uni.removeStorage({
		key: key,
		success: function(res) {
			console.log('清除信息成功');
		}
	});
}
