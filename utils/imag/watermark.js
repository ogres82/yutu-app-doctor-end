import moment from 'moment'
export const waterMark = {
  data() {
    return {
      w: '750px',
      h: '750px',
      waterMarkPath: '',
    }
  },
  methods: {
    getWaterMark(ptah) {
      let that = this

      that.name = that.$store.state.name
      uni.getImageInfo({
        src: ptah,
        success: (ress) => {
          that.w = ress.width / 3 + 'px'
          that.h = ress.height / 3 + 'px'
          let ctx = uni.createCanvasContext('firstCanvas', that) /** 创建画布 */
          //将图片src放到cancas内，宽高为图片大小
          ctx.drawImage(ptah, 0, 0, ress.width / 3, ress.height / 3)
          ctx.setFontSize(15)
          ctx.setFillStyle('#000')
          ctx.rotate((30 * Math.PI) / 180)
          let textToWidth = (ress.width / 3) * 0.4
          let textToHeight = (ress.height / 3) * 0.1

          ctx.fillText(
            `${that.name}${moment().format('YYYY.MM.DD')}`,
            textToWidth,
            textToHeight
          )
          /** 除了上面的文字水印，这里也可以加入图片水印 */
          //ctx.drawImage('/static/watermark.png', 0, 0, ress.width / 3, ress.height / 3)
          console.log(11)
          ctx.draw(false, () => {
            setTimeout(() => {
              uni.canvasToTempFilePath({
                canvasId: 'firstCanvas',
                success: (res1) => {
                  console.log(res1)
                  that.waterMarkPath = res1.tempFilePath
                  that.w = 750 + 'px'
                  that.h = 750 + 'px'
                },
              })
            }, 500)
          })
        },
      })
    },
  },
}
