import {getAppVersion} from "@/api/login";

/**
 * 显示消息提示框
 * @param content 提示的标题
 */
export function toast(content) {
    uni.showToast({
        icon: 'none',
        title: content
    })
}

/**
 * 显示模态弹窗
 * @param content 提示的标题
 */
export function showConfirm(content) {
    return new Promise((resolve, reject) => {
        uni.showModal({
            title: '提示',
            content: content,
            cancelText: '取消',
            confirmText: '确定',
            success: function (res) {
                resolve(res)
            }
        })
    })
}

/**
 * 参数处理
 * @param params 参数
 */
export function tansParams(params) {
    let result = ''
    for (const propName of Object.keys(params)) {
        const value = params[propName]
        var part = encodeURIComponent(propName) + "="
        if (value !== null && value !== "" && typeof (value) !== "undefined") {
            if (typeof value === 'object') {
                for (const key of Object.keys(value)) {
                    if (value[key] !== null && value[key] !== "" && typeof (value[key]) !== 'undefined') {
                        let params = propName + '[' + key + ']'
                        var subPart = encodeURIComponent(params) + "="
                        result += subPart + encodeURIComponent(value[key]) + "&"
                    }
                }
            } else {
                result += part + encodeURIComponent(value) + "&"
            }
        }
    }
    return result
}

export function checkVersion(automatic) {
    if (automatic) {
        //需要加一个延迟，不能一进来就跳转到app在线升级页面，会报错
        setTimeout(() => {
            check()
        }, 1000)
    } else {
        //手动检查版本更新不需要延迟
        check()
    }
}

function check() {
    //获取最新版本号，版本号固定为整数
    getAppVersion().then(res => {
        const newVersionName = res.data.versionName //线上最新版本名
        const newVersionCode = res.data.versionCode; //线上最新版本号
        const selfVersionCode = Number(uni.getSystemInfoSync().appVersionCode) //当前App版本号
        //线上版本号高于当前，进行在线升级
        if (selfVersionCode < newVersionCode) {
            let platform = uni.getSystemInfoSync().platform //手机平台
            //安卓手机弹窗升级
            if (platform === 'android') {
                uni.navigateTo({
                    url: '/pages/upgrade/upgrade'
                })
            } else {
                //IOS无法在线升级提示到商店下载
                uni.showModal({
                    title: '发现新版本 ' + newVersionName,
                    content: '请到App store进行升级',
                    showCancel: false
                })
            }
        } else {
            uni.showToast({
                title: '当前为最新版本',
                icon: 'none'
            });
        }
    })
}

// /**
//  * 通过字典的值过滤得到label
//  * @param {字典值} value
//  * @param {*} dicts  this.dict.type[key]
//  */
// export function getDictLabelByvalue(value, dicts) {
// 	if (label === undefined) {
// 		return "";
// 	}
// 	let value = null;
// 	if (dicts  && !!value) {
// 		for (let item of dicts) {
// 			if (item.label.toString() === label.toString()) {
// 				value = item.value;
// 				break
// 			}
// 		}
// 	}
// 	return value
// }
