import {
	getLastByAppKey
} from "@/api/system/sysAppVersion"
import config from '@/config';
import modal from "@/plugins/modal"
import permision from "@/common/permission.js"
import {
	login
} from "../api/login";
let skipInfo = {
	code: 200,
	msg: "成功",
	data: {
		skip: true
	}
}
export default {
	trytryUpgradeNew(handCheck) { //新版检测更新
		if (permision.isIOS) { //判断如果是IOS系统直接跳过版本检测
			return Promise.resolve(this.getNoSkipInfo());
		}
		this.handCheck = handCheck
		let appKey = config.appInfo.app_key
		let serverVersionInfo = {} //服务端App版本信息
		let localVersionInfo = {} //本地服务信息
		let fileUrl = ""
		let updateType = undefined
		return getLastByAppKey(appKey)
			.then(res => {
				serverVersionInfo = res.data
				fileUrl = serverVersionInfo?.fileUrl
				if (!fileUrl) {
					this.showMsg("已经是最新版本，无需更新！", handCheck)
					return Promise.resolve(skipInfo)
				}
				updateType = this.getUpdateType(fileUrl)
				if (!updateType) {
					this.showMsg("已经是最新版本，无需更新！", handCheck)
					return Promise.resolve(skipInfo)
				}
				// fileUrl = config.baseUrl + fileUrl
				// console.log("fileUrl", fileUrl);
				return this.promiseGetLocalVersionInfo()
			})
			.then(res => {
				if (res.data.skip) {
					uni.$emit('hasAppUpdate', {
						msg: false
					});
					uni.setStorageSync('hasAppUpdate', 0);
					return Promise.resolve(skipInfo)
				}
				localVersionInfo = res.data
				let result = this.compareVersion(serverVersionInfo, localVersionInfo)
				if (result != 1 && handCheck) {
					uni.$emit('hasAppUpdate', {
						msg: false
					});
					uni.setStorageSync('hasAppUpdate', 0);
					this.showMsg("已经是最新版本，无需更新！", this.handCheck)
					return Promise.resolve(skipInfo);
				}
				if (result == 1) { //需要更新
					if (updateType == "wgt") { // 静默更新，只有wgt有
						uni.downloadFile({
							url: fileUrl,
							success: res => {
								if (res.statusCode == 200) { // 下载好直接安装，下次启动生效
									plus.runtime.install(res.tempFilePath, {
										force: false
									});
								}
							}
						});
					} else {
						let info = {
							version: serverVersionInfo.version,
							url: fileUrl,
							type: updateType,
							contents: serverVersionInfo.versionDescribe,
							is_mandatory: serverVersionInfo.must == 'Y' ? true : false
						}
						uni.setStorageSync(appKey, info);
						uni.$emit('hasAppUpdate', {
							msg: true
						});
						uni.setStorageSync('hasAppUpdate', 1);
						uni.navigateTo({
							url: `/components/UpgradeCenter/pages/upgrade-popup?local_storage_key=${appKey}`,
							fail: (err) => {
								console.error('更新弹框跳转失败', err)
								uni.removeStorageSync(appKey)
							}
						})
					}
					return Promise.resolve(this.getNoSkipInfo());
				}
				return Promise.resolve(skipInfo);
			})
			.catch(err => {
				console.log("errr", err);
			})
	},
	tryUpgrade(handCheck) { //尝试检查升级
		this.handCheck = handCheck
		let appKey = config.appInfo.app_key
		let serverVersionInfo = {} //服务端App版本信息
		let localVersionInfo = {} //本地服务信息
		let fileUrl = ""
		let updateType = undefined
		return getLastByAppKey(appKey)
			.then(res => {
				serverVersionInfo = res.data
				fileUrl = serverVersionInfo?.fileList?.[0].filePath
				if (!fileUrl) {
					this.showMsg("已经是最新版本，无需更新！", handCheck)
					return Promise.resolve(skipInfo)
				}
				updateType = this.getUpdateType(fileUrl)
				if (!updateType) {
					this.showMsg("已经是最新版本，无需更新！", handCheck)
					return Promise.resolve(skipInfo)
				}
				fileUrl = config.baseUrl + fileUrl
				return this.promiseGetLocalVersionInfo()
			})
			.then(res => {
				console.log("222222222222", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				localVersionInfo = res.data
				return this.promiseCompareVersion(serverVersionInfo, localVersionInfo)
			})
			.then(res => {
				//console.log("3333333333", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				return this.promiseShowModal(serverVersionInfo)
			})
			.then(res => {
				//console.log("3333333333", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				return this.tryDownLoad(fileUrl)
			})
			.then(res => {
				//console.log("444444444444", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				let filePath = res.data
				return this.tryInstall(filePath)
			})
			.then(res => {
				//console.log("5555555", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				return this.confirmRestart()
			})
			.then(res => {
				//console.log("666666666666", res);
				if (res.data.skip) {
					return Promise.resolve(skipInfo)
				}
				plus.runtime.restart();
			})
			.catch(err => {
				//console.log("更新发生错误：" + err);
				this.showMsg("更新发生错误：" + err, true)
			})
	},
	promiseShowModal(serverVersionInfo) { //Promise方式判断是否要选择更新
		//console.log("serverVersionInfo", serverVersionInfo);
		return new Promise((resolve) => {
			uni.showModal({
				title: '版本更新',
				content: "当前最新版本为“V" + serverVersionInfo.version + "”,是否立即更新？",
				showCancel: serverVersionInfo.must === 'N',
				confirmText: '是',
				cancelText: '否',
				success: (res) => {
					if (res.confirm) {
						resolve(this.getNoSkipInfo())
					} else {
						resolve(skipInfo)
					}
				},
				fail: () => {
					this.showMsg("版本更新发生错误", true)
					resolve(skipInfo)
				}
			})
		})
	},
	promiseGetLocalVersionInfo() { //获取本地程序的版本
		return new Promise((resolve) => {
			// #ifdef APP-PLUS
			plus.runtime.getProperty(plus.runtime.appid, (wgtinfo) => {
				resolve(this.getNoSkipInfo(wgtinfo))
			});
			// #endif
		})
	},
	promiseCompareVersion(serverVersionInfo, localVersionInfo) { //版本号比较
		return new Promise((resolve) => {
			let result = compareVersion(serverVersionInfo, localVersionInfo)
			if (result != 1) {
				this.showMsg("已经是最新版本，无需更新！", this.handCheck)
				resolve(skipInfo)
				return
			}
			resolve(this.getNoSkipInfo())
		})
	},
	compareVersion(serverVersionInfo, localVersionInfo) {
		try {
			let v1 = String(serverVersionInfo.version).split('.')
			let v2 = String(localVersionInfo.version).split('.')
			const minVersionLens = Math.min(v1.length, v2.length);
			let result = 0;
			for (let i = 0; i < minVersionLens; i++) {
				const curV1 = Number(v1[i])
				const curV2 = Number(v2[i])
				if (curV1 > curV2) {
					result = 1
					break;
				} else if (curV1 < curV2) {
					result = -1
					break;
				}
			}
			if (result === 0 && (v1.length !== v2.length)) {
				const v1BiggerThenv2 = v1.length > v2.length;
				const maxLensVersion = v1BiggerThenv2 ? v1 : v2;
				for (let i = minVersionLens; i < maxLensVersion.length; i++) {
					const curVersion = Number(maxLensVersion[i])
					if (curVersion > 0) {
						v1BiggerThenv2 ? result = 1 : result = -1
						break;
					}
				}
			}
			return result;
		} catch (e) {
			//console.log("ffffffff", e);
		}
	},


	tryDownLoad(url) { //尝试下载一个文件
		const downloadTask = null;
		return new Promise((resolve) => {
			const downloadTask = uni.downloadFile({
				url: encodeURI(url), //下载地址
				success: (res) => {
					resolve(this.getNoSkipInfo(res.tempFilePath))
				},
				fail: () => {
					this.showMsg("安装包下载失败", true)
					resolve(skipInfo)
				}
			})
			var showLoading = plus.nativeUI.showWaiting("正在下载"); //创建一个showWaiting对象
			downloadTask.onProgressUpdate((res) => {
				showLoading.setTitle("正在下载" + res.progress + "%  ");
				if (res.progress == 100) {
					plus.nativeUI.closeWaiting();
				}
			});
		})
	},
	getUpdateType(fileUrl) { //获取安装包的类型
		let index = fileUrl.lastIndexOf(".")
		let updateType = undefined
		if (index > -1) {
			updateType = fileUrl.substring(index + 1)
		}
		if (!updateType || (updateType != "apk" && updateType != "wgt")) {
			return undefined
		}
		return updateType;
	},
	tryInstall(filePath) { //尝试安装一个文件
		var showLoading = plus.nativeUI.showWaiting("正在安装。。。");
		return new Promise((resolve) => {
			let options = {
				force: true
			}
			//console.log("tryInstall", filePath, options);
			plus.runtime.install(filePath, options,
				(widgetInfo) => {
					//console.log("widgetInfowidgetInfowidgetInfowidgetInfo'222222222222222");
					plus.nativeUI.closeWaiting();
					resolve(this.getNoSkipInfo(widgetInfo))
					//console.log("widgetInfowidgetInfowidgetInfowidgetInfo'");
				},
				(error) => {
					plus.nativeUI.closeWaiting();
					this.showMsg("安装发生错误" + error, true)
					resolve(skipInfo)
				});
		})
	},
	confirmRestart(updateType) { //安装成功回调
		if (updateType != "wgt") {
			return Promise.resolve(skipInfo)
		}
		return new Promise((resolve) => {
			uni.showModal({
				title: '安装成功',
				content: "安装成功，是否现在重启进行使用？",
				showCancel: true,
				confirmText: "是",
				cancelText: '否',
				success: (res) => {
					if (res.confirm) { //要重启
						resolve(this.getNoSkipInfo())
					} else {
						resolve(skipInfo) //不重起
					}
				},
				fail: (err) => {
					//console.log("confirmRestart发生错误：", err);
					this.showMsg("发生错误：" + err, true)
					resolve(skipInfo) //不重起
				}
			})
		})
	},
	showMsg(contnet, show) {
		if (show === true) {
			modal.msg(contnet)
		}
	},
	getNoSkipInfo(data) {
		// console.log("getNoSkipInfo", data);
		return {
			code: 200,
			msg: "成功",
			data: data || {}
		}
	}
}