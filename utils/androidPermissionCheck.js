//映入插件，插件名称为package.json->_dp_nativeplugin->android->plugins-对应插件的name
let androidPermissionCheck = uni.requireNativePlugin("Uniplugin-AndroidPermissionCheck");
let cameraTitle =
	"需要获取“摄像头权限”、“本地存储权限”、“相册权限”，将用于拍摄照片或视频；从本地选择照片照片或视频。这样，您可以在应用程序中发布带图文章、给我们提意见、设置或修改的头像、上传受检者信息、扫码获取机构信息、上传证件信息、订单信息、编辑信息等！若允许授权轻工点击‘确认’按钮进行授权！"
let permissionList = [{
	name: "CAMERA",
	permission: "android.permission.CAMERA",
	content: cameraTitle
}, {
	name: "camera",
	permission: "android.hardware.camera",
	content: cameraTitle
}, {
	name: "autofocus", //应用程序使用设备的相机的自动对焦能力
	permission: "android.hardware.camera.autofocus",
	content: cameraTitle
}, {
	name: "ACCESS_COARSE_LOCATION",
	permission: "android.permission.ACCESS_COARSE_LOCATION",
	content: cameraTitle
}, {
	name: "ACCESS_FINE_LOCATION", //大致位置信息
	permission: "android.permission.ACCESS_FINE_LOCATION",
	content: cameraTitle
}, {
	name: "ACCESS_LOCATION_EXTRA_COMMANDS",
	permission: "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS",
	content: cameraTitle
}, {
	name: "BLUETOOTH",
	permission: "android.permission.BLUETOOTH",
	content: "系统需要获得蓝牙权限用于进行蓝牙设备连接，请同意！"
}, {
	name: "CALL_PHONE",
	permission: "android.permission.CALL_PHONE",
	content: "系统需要获取拨号权限用于直接拨打电话，请同意！"
}, {
	name: "CHANGE_NETWORK_STATE", // 检查网络状态和监听网络状态变化
	permission: "android.permission.CHANGE_NETWORK_STATE",
	content: "",

}]
export default {
	getPermissionItem(permissionName) {
		for (let item of permissionList) {
			if (permissionName === item.name) {
				return item
			}
		}
		return null
	},
	/**
	 * @param {Object} 设置某一个权限已经弹框提示过用户
	 */
	alreadyShowModalPermission(permissionName) {
		for (let item of permissionList) {
			if (permissionName === item.name) {
				item.showModal = true
			}
		}
	},
	/**
	 * 使用Promise方式检查权限
	 * @param permissionName
	 * @returns {Promise<unknown>}
	 */
	promiseCheck(permissionName) {
		console.log("promiseCheck",permissionName);
		let permissionItem = this.getPermissionItem(permissionName)
			console.log("getPermissionItem",permissionItem);
		return new Promise((resolve, reject) => {
			console.log("permissionItem.showModal",permissionItem.showModal);
			if (permissionItem.showModal !== true) {
				this.promiseConfirm(permissionItem.content)
					.then(res => {
						if (res) {
							this.alreadyShowModalPermission(permissionItem.name)
						}
						resolve(res)
					})
					.catch(err => {
						resolve(false)
					})
				return
			}
			plus.android.requestPermissions(
				[permissionItem.permission],
				(res) => { //成功的回调
					let permission = permissionItem.permission
					let granted = res.granted
					if (granted.indexOf(permission) != -1) {
						resolve(true)
						return
					}
					let deniedPresent = res.deniedPresent
					let deniedAlways = res.deniedAlways
					if (deniedPresent.indexOf(permission) != -1 ||
						deniedAlways.indexOf(permission) != -1) {
						this.promiseConfirm("权限被拒绝，请打开系统设置配置权限")
							.then(res => {
								resolve(false)
							})
							.catch(err => {
								resolve(false)
							})

						return
					}
				},
				(err) => { //失败的回调
					this.promiseConfirm("权限被拒绝，请打开系统设置配置权限")
						.then(res => {
							resolve(false)
						})
						.catch(err => {
							resolve(false)
						})
				}
			);
		})
	},
	/**
	 * 弹出一个对话框
	 * @param {Object} content
	 */
	promiseConfirm(content) {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title: '系统提示',
				content: content,
				cancelText: '稍后',
				confirmText: '确定',
				success: (res) => {  
					resolve(res.confirm)
				},  
				fail: (err) => {
					resolve(false)
				}
			})
		})
	}
}