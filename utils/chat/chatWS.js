export class DataContent {
	constructor(action, chatMsg, extend) {
		this.action = action;
		this.chatMsg = chatMsg;
		this.extend = extend;
	}
}

// export class ChatMsg {
// 	constructor(senderId, receiverId, msId, msg) {
// 		this.senderId = senderId;
// 		this.receiverId = receiverId;
// 		this.msId = msId;
// 		this.msg = msg;
// 	}
// }

export class ChatMsg {
	constructor(sendId, receiveId, msgId, content,avatar,userName) {
		this.sendId = sendId; //发送人
		this.receiveId = receiveId; //接收人
		this.msgId = msgId; //消息ID
		this.content = content; //发送消息
		this.avatar = avatar;//发送人头像
		this.userName = userName;//发送人头像
	}
}

export const ChatAction = {
	CONNECT: 1, //连接
	CHAT: 2, //聊天
	SIGNED: 3, //签收信息
	KEEPLIVE: 4, //心跳类型
	PULL_FRIEND: 5,
}
