const constant = {
	avatar: 'vuex_avatar',
	name: 'vuex_name',
	roles: 'vuex_roles',
	permissions: 'vuex_permissions',
	mp3Path: 'mp3Path',
	imagePaths: 'imagePaths',
	externalUUid: 'bacc02765e74449183f7a5b30767183a',
	userInfo: 'vuex_userInfo',
	jwd: 'vuex_jwd',
	cartIds: 'vuex_cartIds',
	products: 'vuex_products',
	loginInfo: 'vuex_loginInfo',
}

export default constant