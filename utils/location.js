import {
	geocodeRegeo
} from '@/api/biz/amap'
/**
 * 打开地图工具开始导航
 * @param {Object} latitude
 * @param {Object} longitude
 * @param {Object} address
 */
export function openLocation(latitude, longitude, address) {
	return new Promise((resolve, reject) => {
		uni.openLocation({
			latitude: latitude,
			longitude: longitude,
			address: address,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject("获取位置信息失败，无法使用位置功能！")
			}
		});
	})
}
/**
 * 打开高德改变位置页面获取位置
 */
export function chooseLocation() {
	console.log("chooseLocation");
	return new Promise((resolve, reject) => {
		uni.chooseLocation({
			success: (res) => {
				console.log("chooseLocation", res);
				resolve(res)
			},
			fail: (err) => {
				console.log("chooseLocation==err", err);
				reject("获取位置信息失败，无法使用位置功能！")
			}
		});
	})
}

/**
 * 通过经纬度进行逆地理编码
 * @param {Object} longitude
 * @param {Object} latitude
 */
export function aMapGeocodeRegeo(longitude, latitude) {
	return geocodeRegeo(longitude, latitude)
}

/**
 * 获取位置Promise方法
 */
export function getLocationPromise() {
	console.log("getLocationPromise");
	return new Promise((resolve, reject) => {
		uni.getLocation({
			type: 'gcj02',
			success: function(res) {
				console.log("getLocation===success", res);
				resolve(res)
			},
			fail: function(err) {
				console.log("getLocation===fail", err);
				reject("获取位置失败")
			}
		});
	})
}
/**
 * 打开高德改变位置页面获取位置
 */
export function chooseLocationPromise(point) {
	console.log("chooseLocationPromise", point);
	return new Promise((resolve, reject) => {
		uni.chooseLocation({
			latitude: point.latitude,
			longitude: point.longitude,
			success: (res) => {
				console.log("chooseLocation==success", res);
				resolve(res)
			},
			fail: (err) => {
				console.log("chooseLocation==err", err);
				reject("获取位置信息失败，无法使用位置功能！")
			}
		});
	})
}
 
export function chooseLocationAndRegeo() {
	let location = {}
	return getLocationPromise()
		.then(res => {
			return chooseLocationPromise(res)
		})
		.then(res => {
			location = res
			return geocodeRegeo(res.longitude, res.latitude)
		})
		.then(res => {
			return geocode2SysAddressPromise(res.data)
		})
		.then(res => {
			return mergeLocationAndSysAddress(location, res)
		})
		.catch(err => {
			uni.showToast({
				title: err,
				icon: 'none'
			})
			return new Promise((resolve, reject) => {
				reject(err)
			})
		})
}

/**
 * 高德逆地理编码得到结果转系统规定的地址信息
 */
function geocode2SysAddress(regeocode) {
	let addressComponent = regeocode.addressComponent
	let adcode = addressComponent.adcode //行政区编码
	return {
		provinceId: parseInt(adcode / 10000) + "0000",
		provinceName: addressComponent.province, //坐标点所在省名称 
		cityId: parseInt(adcode / 100) + "00",
		cityName: addressComponent.city.length == 0 ? "" : addressComponent.city, //坐标点所在城市名称
		regionId: adcode,
		regionName: addressComponent.district, //坐标点所在区
	}
}

function geocode2SysAddressPromise(regeocode) {
	console.log("geocode2SysAddressPromise", regeocode);
	return new Promise((resolve, reject) => {
		let addressInfo = geocode2SysAddress(regeocode)
		resolve(addressInfo)
	})
}
/**
 * 合并位置选择结果和逆向地址解析出来的系统使用地址
 * @param {Object} regeocode
 */
function mergeLocationAndSysAddress(location, sysAddress) {
	console.log("mergeLocationAndSysAddress", location, sysAddress);
	return new Promise((resolve, reject) => {
		let addressInfo = {
			code: sysAddress.regionId,
			name: sysAddress.regionName,
			allData: sysAddress,
			...location
		}
		resolve(addressInfo)
	})
}