// 应用全局配置
module.exports = {
	// baseUrl: 'http://192.168.1.118:8080',
	baseUrl: 'https://web.lfonehealth.com/prod-api',         
	nirvanaPns: {
		android: 'x8FLESnQg5CTAtBSI164jTq8+tUaaFnL8J2uuTkGVIXUDQ3a3dkulKFaIjBml75n1JsurMmw/eLII7zKQ+5JkV3oxE/f0cXnb8DNpI8KvHwhwhJEDnOS+MWwbOJy125I5qARhXrSKBGC9rJBafLusCjoHpLNxpJ274Obn0T2xrRNRGyzQEX2DOsO6oKmJK/8X75uuytKJBe7dnrEnzrlQ+KvwnJOOkqqfatbj7C31vMpFJYM1BSI2hmEIkb/7nUqGt0CA/+3MlVipboQFp6IdGYHPRL0QROg0xAAJHtlgfs=',
		yhxy: "https://web.lfonehealth.com/plus/#/agreement?titleEn=ysrzxy&type=app",
		ysxy: "https://web.lfonehealth.com/plus/#/agreement?titleEn=ysyszc&type=app"
	},
	// 应用信息
	appInfo: {
		app_key: "YUTU_DOCTOR_ANDROID",
		// 政策协议
		agreements: [{
				title: "隐私政策",
				url: "https://ruoyi.vip/protocol.html"
			},
			{
				title: "用户服务协议",
				url: "https://ruoyi.vip/protocol.html"
			}
		]
	}
}