import Vue from 'vue'
import App from './App'
import VueI18n from 'vue-i18n'
import messages from './locale/index'


let i18nConfig = {
	locale: uni.getLocale(),
	messages
}

Vue.use(VueI18n)
const i18n = new VueI18n(i18nConfig)


import information from './pages/information/information.vue'
Vue.component('information', information)

import informationRE from './pages/information/information-re.vue'
Vue.component('informationre', informationRE)

import serviceRE from './pages/service/service-re.vue'
Vue.component('servicere', serviceRE)

import service from './pages/service/service.vue'
Vue.component('service', service)

import cart from './pages/cart/cart.vue'
Vue.component('cart', cart)

import work from './pages/work/work.vue'
Vue.component('work', work)


import nurseWork from './pages/work/nurseWork.vue'
Vue.component('nurseWork', nurseWork)



import tidings from './pages/tidings/tidings.vue'
Vue.component('tidings', tidings)

import my from './pages/my/my.vue'
Vue.component('my', my)

import myRe from './pages/my/my-re.vue'
Vue.component('myre', myRe)

import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom', cuCustom)

import heart6Heard from './colorui/components/heart6-heard.vue'
Vue.component('heart6-heard', heart6Heard)


import ajax from './utils/request/ajax.js'
Vue.prototype.$ajax = ajax

import store from './store'
Vue.prototype.$store = store //添加$store(若依)

import plugins from './plugins' 
Vue.use(plugins)//添加插件（若依）

import appPush from './utils/tankuang/tk.js'
Vue.use(appPush)

// import store from './store/index.js'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	i18n,
	...App,
	// store
})
app.$mount()
