import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/modules/user'
import dict from '@/store/modules/dict'
import getters from '@/store/getters'


Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		user,
		dict,
	},
	state: {
		deviceBLu: false, //0为断开1为连接
		latitude: null, //经度
		longitude: null, //纬度
		altitude: null, //高程
		positioningTime: null, //定位时间
		lastpositioningTime: null, //最后定位时间
		positionState: false, //定位状态
		carId: null, //北斗卡号
		carFrequentness: null, //卡频度
		carCommunicationLength: null, //通信长度
		receivedMsg: null, //收到的信息
		receiveAddress: null, //收到消息的发送者
		deviceId: null, //蓝牙uuid
		serviceId: null, //蓝牙uuid
		writeCharacteristicId: null, //蓝牙uuid
		receivedMsgState: false, //接收消息状态
		satelliteAmount0: null, //RDSS波束
		satelliteAmount1: null, //RNSS卫星数
		alarmStatus: false, //报警状态
		batteryPercentage: 0, //电池电量百分比
		remainder: 0, //发送剩余时间
		sendFeedback: {}, //发送反馈
		messages: [], //聊天消息记录
		originalLongitude: null, //原始经度
		originalLatitude: null, //原始纬度
		managementStatus: false, //打点状态
		userInfo: {}
	},
	mutations: {
		userInfo(state, val) {
			state.userInfo = val
		}
	},
	getters
})

export default store
