import config from '@/config'
import storage from '@/utils/storage'
import constant from '@/utils/constant'
import {
	login,
	logout,
	getInfo
} from '@/api/login'
import {
	getToken,
	setToken,
	removeToken
} from '@/utils/auth'
import {
	listDept,
} from '@/api/system/dept'

const baseUrl = config.baseUrl

const user = {
	state: {
		token: getToken(),
		name: storage.get(constant.name),
		avatar: storage.get(constant.avatar),
		roles: storage.get(constant.roles),
		permissions: storage.get(constant.permissions),
		scopeDeptInfoList: storage.get(constant.scopeDeptInfoList), //权限范围内的部门信息
		userInfo: storage.get(constant.userInfo), //权限范围内的部门信息
	},

	mutations: {
		SET_TOKEN: (state, token) => {
			state.token = token
		},
		SET_NAME: (state, name) => {
			state.name = name
			storage.set(constant.name, name)
		},
		SET_AVATAR: (state, avatar) => {
			state.avatar = avatar
			storage.set(constant.avatar, avatar)

			state.userInfo.avatar = avatar
			storage.set(constant.userInfo, state.userInfo)
		},
		SET_ROLES: (state, roles) => {
			state.roles = roles
			storage.set(constant.roles, roles)
		},
		SET_PERMISSIONS: (state, permissions) => {
			state.permissions = permissions
			storage.set(constant.permissions, permissions)
		},
		SET_USERINFO: (state, userInfo) => {
			state.userInfo = userInfo
			storage.set(constant.userInfo, userInfo)
		},
		SET_SCOPE_DEPTINFOLIST: (state, deptInfoList) => { //设置权限范围内的部门列表
			state.scopeDeptInfoList = deptInfoList;
			storage.set(constant.scopeDeptInfoList, deptInfoList)
		},
	},
	actions: {
		// 登录   
		Login({
			commit
		}, userInfo) {
			const userName = userInfo.userName
			const password = userInfo.password
			const code = userInfo.code
			const uuid = userInfo.uuid
			return new Promise((resolve, reject) => {
				login(userName, password, code, uuid)
					.then(res => {
						setToken(res.token)
						commit('SET_TOKEN', res.token)
						resolve()
					}).catch(error => {
						reject(error)
					})
			})
		},
		// 获取用户信息
		GetInfo({
			commit,
			state
		}) {
			return new Promise((resolve, reject) => {
				getInfo()
					.then(res => {
						console.log("getInfo",res);
						try {
							uni.setStorageSync('userInfo', res);
						} catch (e) {
							// error
						}
						const user = res.user
						const avatar = (user == null || user.avatar == "" || user.avatar == null) ?
							require("@/static/login/logo.png") : user.avatar
						user.avatar = avatar
						const userName = (user == null || user.userName == "" || user.userName ==
							null) ? "" : user.userName
						if (res.roles && res.roles.length > 0) {
							commit('SET_ROLES', res.roles)
							commit('SET_PERMISSIONS', res.permissions)
						} else {
							commit('SET_ROLES', ['ROLE_DEFAULT'])
						}
						commit('SET_NAME', userName)
						commit('SET_USERINFO', user);
						commit('SET_AVATAR', avatar)
						return listDept({}) //查询范围内的deptList
						// resolve(res)
					})
					.then(res => {
						commit('SET_SCOPE_DEPTINFOLIST', res.data);
						console.log("res==", res.data);
						resolve()
					})
					.catch(error => {
						reject("错了", error)
					})

			})
		},

		// 退出系统
		LogOut({
			commit,
			state
		}) {
			return new Promise((resolve, reject) => {
				logout(state.token).then(() => {
					commit('SET_TOKEN', '')
					commit('SET_ROLES', [])
					commit('SET_PERMISSIONS', [])
					commit('SET_NAME', "")
					commit('SET_AVATAR', "")
					commit('SET_USERINFO', {});

					removeToken()
					storage.clean()
					//取消长连接
					// let newChat = uni.getStorageSync("newChat")
					// uni.removeStorageSync('chatMessages' + this.userName)
					resolve()
				}).catch(error => {
					reject(error)
				})
			})
		}
	}
}

export default user