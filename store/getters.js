const getters = {
	token: state => state.user.token,
	avatar: state => state.user.avatar,
	name: state => state.user.name,
	roles: state => state.user.roles,
	permissions: state => state.user.permissions,
	scopeDeptInfoList: state => state.user.scopeDeptInfoList, //范围内的部门
	userInfo:state =>  state.user.userInfo, //当前登录人的信息
}
export default getters
